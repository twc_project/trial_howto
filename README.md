# TWC: Download, Build, Install

Read all the way through before beginning.

These instructions will download, build and install everything within
a single directory tree.  To purge, remove the top directory.

## Prepare

The complete TWC Project consists of several repositories[^1].  We
recommend using a single 'trial' directory in which to download, build
and install all project files.  For example,

```
    TWC_TRIAL_PATH=/home/<user>/TWC_trial
```
Then

```
    mkdir -p ${TWC_TRIAL_PATH}
    cd       ${TWC_TRIAL_PATH}

    mkdir TWC_project
    mkdir build
    mkdir stage        # the install destination

```

The TWC_project directory is for downloading source repositories.  The
others are for building and installing.  To remove all project files,
including installed files, simply remove the trial directory[^2].

## Download

```
    cd ${TWC_TRIAL_PATH}/TWC_project
    TWC_PROJ_UPSTREAM_BASE=https://gitlab.com/twc_project

    git clone ${TWC_PROJ_UPSTREAM_BASE}/trial_howto

    git clone ${TWC_PROJ_UPSTREAM_BASE}/agent_protocol
    git clone ${TWC_PROJ_UPSTREAM_BASE}/bic_scanner
    git clone ${TWC_PROJ_UPSTREAM_BASE}/courtesy_librarys
    git clone ${TWC_PROJ_UPSTREAM_BASE}/libtwc

    git clone ${TWC_PROJ_UPSTREAM_BASE}/twc

    git clone ${TWC_PROJ_UPSTREAM_BASE}/plug-ins/smpl
    git clone ${TWC_PROJ_UPSTREAM_BASE}/plug-ins/test0
    git clone ${TWC_PROJ_UPSTREAM_BASE}/plug-ins/ctwc
    git clone ${TWC_PROJ_UPSTREAM_BASE}/plug-ins/tmpl

```

The directory structure should look something like this.  The
subdirectories of TWC_project should also be populated.

```
    TWC_trial/
    |-- TWC_project
    |   |-- README.md
    |   |-- agent_protocol
    |   |-- bic_scanner
    |   |-- courtesy_librarys
    |   |-- libtwc
    |   |-- plug-ins
    |   |-- trial_howto
    |   '-- twc
    |-- build
    `-- stage
```

## Dependencies

The TWC Project has been developed on Debian13/Trixie. The
pkg-config dependencies are:

```
    - pkgconf
    - meson
    - ninja-build

    - wayland-server
    - wayland-client
    - wlroots
    - libinput
    - xkbcommon
    - libdrm
    - pixman-1
    - egl
    - wayland-egl

    - wayland-protocols     # /usr/share/wayland-protocols/...
    - wayland_scanner
```

The trial_howto directory contains a useful makefile.  It can do a
dependency check.

```
    cd ${TWC_TRIAL_PATH}/trial_howto
    make dependency_check
```

If there are missing dependencies, try these packages

```
    apt-get install gcc pkgconf make meson ninja-build
    apt-get install libwayland-bin
    apt-get install wayland-protocols
    apt-get install libwlroots-dev
```

The wlroots package should bring in most other needed packages.

## Meson setup and build

In addition to the dependency_check, the Makefile in the trial_howto
directory can also setup the Meson build systems for each repository.
The Makefile uses the following shell variable.

```
    export TWC_TRIAL_PATH=/home/<user>/TWC_trial

    make from_scratch
```

Hint: You can copy and edit the Makefile to customize the make
variable TWC_TRIAL_PATH.  It's on the first non-comment line.  Also
note that the Makefile defines all pertinent directories based on this
one variable.  Therefore, you can copy the Makefile to any convenient
location and it will still work.  (The file Makefile_build is only
needed by the from_scratch target.)

To start over, simply remove the build and stage directories.

In addition to meson setup, the from_scratch target also performs an
initial build and install.  For a subsequent build/install simply
invoke make.

```
    make
```

To mimic the example in the first [tutorial] run

```
    ${PREFIX}/bin/smpl_compositor
```

This runs a compositor with the smpl client statically linked as
Built-in Client.

You can then run smpl as a regular external client against the above
compositor by executing

```
    ${PREFIX}/bin/smpl_client
```

[^1]: Yes, there are many repositories.  This approach allows
      developers to selectively access independent portions of the TWC
      Project.  Some may only be interested in the Agent Protocol.
      Some may also want the server library.  While still others may
      only be interested the plug-ins.

[^2]: The provided makefiles use the 'PREFIX' feature to ensure that
      install destinations are under the ./TWC_trial/stage/ directory.

#### Installed Listing

Below is a partial listing after all repositories have been built and
installed into a common staging directory.

The bin directory contains two external clients and three compositors.
The twc compositor derives its' personality completely through
plug-ins.  Neither the smpl_compositor nor the ctwc_compositor accept
plug-ins, instead deriving their personality solely from statically
linked built-in clients.

In the include directory, one can see the two sets of header files for
clients.  One set for compiling as a standard client and one set for
compiling as a built-in client.  Client source code can be arranged to
be identical in each case - the only difference is the include
directory specified on the compile command line.

The plug-ins directory hints at the various agents in the CTWC window
manager.  The shared object libctwc-plg.so is a multi-module plug-in
containing all the CTWC agents.  The single-agent versions are
available in case a user wants to mix and match agents from several
sources.

    stage/      ( ${PREFIX} or /usr/local/ )
    |-- bin
    |   |-- ctwc_client
    |   |-- ctwc_compositor
    |   |-- smpl_client
    |   |-- smpl_compositor
    |   `-- twc
    |-- include
    |   |-- twc
    |   |   |-- twc_client_initialize.h
    |   |   |-- twc-server.h
    |   |   |-- wayland_types.h
    |   |   |-- wl_DeVault.h
    |   `-- twc-protocols
    |       |-- built-in
    |       |   |-- wp-desktop-cues-v1-client-protocol.h
    |       |   |-- wayland-client-protocol.h
    |       |   |-- xdg-agent-v1-client-protocol.h
    |       |   |-- xdg-decoration-unstable-v1-client-protocol.h
    |       |   `-- xdg-shell-client-protocol.h
    |       `-- standard
    |           |-- wp-desktop-cues-v1-client-protocol.h
    |           |-- wayland-client-protocol.h
    |           |-- xdg-agent-v1-client-protocol.h
    |           |-- xdg-decoration-unstable-v1-client-protocol.h
    |           `-- xdg-shell-client-protocol.h
    `-- lib
    |   `-- x86_64-linux-gnu
    |       |-- pkgconfig
    |       |   |-- twc-client-protocol-libs.pc
    |       |   |-- twc-protocols-bic.pc
    |       |   |-- twc-protocols-std.pc
    |       |   `-- twc-server.pc
    |       `-- twc
    |           |-- libwp-desktop_cues-v1-client.a
    |           |-- libtwc.a
    |           |-- libwayland-client.a
    |           |-- libxdg-agent-v1-client.a
    |           |-- libxdg-decoration-unstable-v1-client.a
    |           |-- libxdg-shell-client.a
    |           `-- plugins
    |               |-- libctwc-plg.so
    |               |-- libctwc_deco-plg.so
    |               |-- libctwc_geom-plg.so
    |               |-- libctwc_icon-plg.so
    |               |-- libctwc_icon_mgr-plg.so
    |               |-- libctwc_menu-plg.so
    |               |-- libctwc_wksp-plg.so
    |               `-- libsmpl-plg.so
    `-- share
        |-- pkgconfig
        |   `-- agent-protocols.pc
        `-- wayland-protocols
            `-- experimental
                |-- desktop_cues
                |   `-- wp-desktop-cues-v1.xml
                `-- xdg-agent
                    `-- xdg-agent-v1.xml

[tutorial1]: https://gitlab.com/twc_project/tutorials/tutorial1_BiC.md
