
  #
  # This makefile works with the follwing directory structure.  The
  # subdirectories of TWC_project are popluated using 'git clone ...'
  #
  #   ./TWC_trial/
  #   |-- TWC_project/
  #   |   |-- trial_howto/
  #   |   |   ` -- Makefile        # this file
  #   |   |-- agent_protocol/
  #   |   |-- bic_scanner/
  #   |   |-- courtesy_librarys/
  #   |   |-- libtwc/
  #   |   |-- twc/
  #   |   `-- plug-ins/
  #   |-- build/  . . .
  #   `-- stage/  . . .
  #
  # The TWC_project directory contains sources.  The build and stage
  # directories are created by the 'from_scratch' target below.
  #
  # Synopsis:
  #
  # Set a shell variable (or edit the TWC_TRIAL_PATH line below.)
  #
  #   export TWC_TRIAL_PATH=/home/twc/TWC_trial
  #
  #   cd ${TWC_TRIAL_PATH}/TWC_project/trial_howto
  #
  # Check for dependencies.
  #
  #   make dependency_check
  #
  # The initial build invocation (to setup all the meson build
  # directories):
  #
  #   make from_scratch
  #
  # Subsequently:
  #
  #   make
  #

  # Edit the following variable to simplify your make command line.
  # Or set the shell variable as shown above.
TWC_TRIAL_PATH ?= /home/twc/TWC_trial
PREFIX         ?= $(TWC_TRIAL_PATH)/stage

TWC_PROJECT_PATH := $(TWC_TRIAL_PATH)/TWC_project
 BUILD_BASE_PATH := $(TWC_TRIAL_PATH)/build

PKG_CONFIG_PATH := $(PREFIX)/lib/x86_64-linux-gnu/pkgconfig:$(PREFIX)/share/pkgconfig

  AGENT_PROTOCOL_SRC_PATH := $(TWC_PROJECT_PATH)/agent_protocol
     BIC_SCANNER_SRC_PATH := $(TWC_PROJECT_PATH)/bic_scanner
COUTESY_LIBRARYS_SRC_PATH := $(TWC_PROJECT_PATH)/courtesy_librarys
          LIBTWC_SRC_PATH := $(TWC_PROJECT_PATH)/libtwc
             TWC_SRC_PATH := $(TWC_PROJECT_PATH)/twc
        PLUG-INS_SRC_PATH := $(TWC_PROJECT_PATH)/plug-ins

  AGENT_PROTOCOL_BUILD_BASE := $(BUILD_BASE_PATH)/agent_protocol
     BIC_SCANNER_BUILD_BASE := $(BUILD_BASE_PATH)/bic_scanner
COUTESY_LIBRARYS_BUILD_BASE := $(BUILD_BASE_PATH)/courtesy_librarys
          LIBTWC_BUILD_BASE := $(BUILD_BASE_PATH)/libtwc
             TWC_BUILD_BASE := $(BUILD_BASE_PATH)/twc
        PLUG-INS_BUILD_BASE := $(BUILD_BASE_PATH)/plug-ins

AGENT_PROTOCOL_BUILD_DIR := \
        $(AGENT_PROTOCOL_BUILD_BASE)/build

BIC_SCANNER_BUILD_DIRS := \
        $(BIC_SCANNER_BUILD_BASE)/build_std \
        $(BIC_SCANNER_BUILD_BASE)/build_bic

COUTESY_LIBRARYS_BUILD_DIR := \
        $(COUTESY_LIBRARYS_BUILD_BASE)/build

LIBTWC_BUILD_DIR := \
        $(LIBTWC_BUILD_BASE)/build \

TWC_BUILD_DIR := \
        $(TWC_BUILD_BASE)/build

PLUG-INS_BUILD_DIRS := \
        $(PLUG-INS_BUILD_BASE)/smpl/build  \
        $(PLUG-INS_BUILD_BASE)/test0/build \
        $(PLUG-INS_BUILD_BASE)/ctwc/build  \
        $(PLUG-INS_BUILD_BASE)/tmpl/build  \

  # This is the default target.  Prerequisites are listed in required
  # build order.
build:                      \
  AGENT_PROTOCOL_BUILD_DIR  \
     BIC_SCANNER_BUILD_DIRS \
COUTESY_LIBRARYS_BUILD_DIR  \
          LIBTWC_BUILD_DIR  \
             TWC_BUILD_DIR  \
        PLUG-INS_BUILD_DIRS

  # In this rule, each target is also a variable name which contains a
  # list of build directories for that target.
  AGENT_PROTOCOL_BUILD_DIR  \
     BIC_SCANNER_BUILD_DIRS \
COUTESY_LIBRARYS_BUILD_DIR  \
          LIBTWC_BUILD_DIR  \
             TWC_BUILD_DIR  \
        PLUG-INS_BUILD_DIRS  :
	@ # iterate though the targets' directory list.
	@for d in $($@); do        \
	    echo ;		   \
	    echo "-- $$d";	   \
	    $(MAKE)		   \
	      --no-print-directory \
	      --directory=$$d	   \
	      -f Makefile_build	   \
	      build;		   \
	done

clean:
	@for d in                          \
	      $(AGENT_PROTOCOL_BUILD_DIR)  \
	         $(BIC_SCANNER_BUILD_DIRS) \
	    $(COUTESY_LIBRARYS_BUILD_DIR)  \
	              $(LIBTWC_BUILD_DIR)  \
	                 $(TWC_BUILD_DIR)  \
	            $(PLUG-INS_BUILD_DIRS) \
	; do                       \
	    echo "-- $$d";	   \
	    $(MAKE)		   \
	      --no-print-directory \
	      --directory=$$d	   \
	      -f Makefile_build	   \
	      $@;		   \
	done


 # -------------
 # Initial Setup
 #
 # The following targets are used to create and setup the meson build
 # directories.  These targets should only be needed once.  They are
 # listed in the required invocation order.
 #
 # Each target of the form <target>_init performs a meson setup AND a
 # ninja build/install.  This is because successive meson setups
 # depend on prior installs.  The 'from_scratch' target will do them
 # all in proper order.
 #

 #
 # create build directories
 #

create_build_dirs:
	@for d in                          \
	      $(AGENT_PROTOCOL_BUILD_DIR)  \
	         $(BIC_SCANNER_BUILD_DIRS) \
	    $(COUTESY_LIBRARYS_BUILD_DIR)  \
	              $(LIBTWC_BUILD_DIR)  \
	                 $(TWC_BUILD_DIR)  \
	            $(PLUG-INS_BUILD_DIRS) \
	; do                       \
	    mkdir -p $$d ;         \
	    echo "mkdir -p $$d";   \
	    cp Makefile_build $$d; \
	done

 #
 # agent_protocol
 #

agent_protocol_setup:
	@meson setup --prefix $(PREFIX)  $(AGENT_PROTOCOL_BUILD_DIR)  $(AGENT_PROTOCOL_SRC_PATH)

agent_protocol_init:	agent_protocol_setup AGENT_PROTOCOL_BUILD_DIR

 #
 # bic_scanner
 #

bic_scanner_setup:
	meson setup --prefix $(PREFIX) -Dbuild_type=standard -Dpkg_config_path=$(PKG_CONFIG_PATH) $(BIC_SCANNER_BUILD_BASE)/build_std $(BIC_SCANNER_SRC_PATH)
	meson setup --prefix $(PREFIX) -Dbuild_type=built-in -Dpkg_config_path=$(PKG_CONFIG_PATH) $(BIC_SCANNER_BUILD_BASE)/build_bic $(BIC_SCANNER_SRC_PATH)

bic_scanner_init:	bic_scanner_setup BIC_SCANNER_BUILD_DIRS

 #
 # courtesy_librarys
 #

courtesy_librarys_setup:
	meson setup --prefix $(PREFIX) -Dpkg_config_path=$(PKG_CONFIG_PATH) $(COUTESY_LIBRARYS_BUILD_DIR) $(COUTESY_LIBRARYS_SRC_PATH)

courtesy_librarys_init:	courtesy_librarys_setup COUTESY_LIBRARYS_BUILD_DIR

 #
 # libtwc
 #

libtwc_setup:
	meson setup --prefix $(PREFIX) -Dpkg_config_path=$(PKG_CONFIG_PATH)  $(LIBTWC_BUILD_DIR)  $(LIBTWC_SRC_PATH)

libtwc_init:	libtwc_setup LIBTWC_BUILD_DIR

 #
 # twc
 #

twc_setup:
	meson setup --prefix $(PREFIX) -Dpkg_config_path=$(PKG_CONFIG_PATH)  $(TWC_BUILD_DIR)  $(TWC_SRC_PATH)

twc_init:	twc_setup TWC_BUILD_DIR

 #
 # plug-ins
 #

plug-ins_setup:
	meson setup --prefix $(PREFIX) -Dpkg_config_path=$(PKG_CONFIG_PATH)  $(PLUG-INS_BUILD_BASE)/smpl/build   $(PLUG-INS_SRC_PATH)/smpl
	meson setup --prefix $(PREFIX) -Dpkg_config_path=$(PKG_CONFIG_PATH)  $(PLUG-INS_BUILD_BASE)/test0/build  $(PLUG-INS_SRC_PATH)/test0
	meson setup --prefix $(PREFIX) -Dpkg_config_path=$(PKG_CONFIG_PATH)  $(PLUG-INS_BUILD_BASE)/ctwc/build   $(PLUG-INS_SRC_PATH)/ctwc
	meson setup --prefix $(PREFIX) -Dpkg_config_path=$(PKG_CONFIG_PATH)  $(PLUG-INS_BUILD_BASE)/tmpl/build   $(PLUG-INS_SRC_PATH)/tmpl

plug-ins_init:	plug-ins_setup PLUG-INS_BUILD_DIRS

 #
 # Everything
 #

from_scratch:	create_build_dirs agent_protocol_init bic_scanner_init courtesy_librarys_init libtwc_init twc_init plug-ins_init


 # ----------------------
 # Check for dependencies
 #

COMMAND_DEPENDENCIES := \
    pkg-config \
    meson      \
    ninja

PKGCONF_DEPENDENCIES := \
    wayland-protocols \
    wayland-server    \
    wayland-client    \
    wlroots           \
    libdrm            \
    libinput          \
    xkbcommon         \
    pixman-1          \
    egl               \
    wayland-egl

dependency_check:
	@for c in $(COMMAND_DEPENDENCIES) ; do \
	    printf "%-20s" "$$c" ; ( [ -x "$$(command -v $$c)" ] && { printf "    Found\n" ; } || { printf "not Found\n" ; } ) \
	done
	@for p in $(PKGCONF_DEPENDENCIES) ; do \
	    printf "%-20s" "$$p"   ; (pkg-config --exists $$p    && { printf "    Found\n" ; } || { printf "not Found\n" ; } ) \
	done

SYSLIB_DIR := "/lib/x86_64-linux-gnu"

  # Incomplete.  Try ldd ${PREFIX}/stage/bin/smpl_client
SYSLIB_DEPENDENCIES := \
    libseat.so.1    \
    libevdev.so.2   \
    libgcrypt.so.20 \
    liblzma.so.5

syslib_check:
	@for l in $(SYSLIB_DEPENDENCIES) ; do \
	    printf "%-20s" "$$l" ; ( [ -e "$(SYSLIB_DIR)/$$l" ] && { printf "    Found\n" ; } || { printf "not Found\n" ; } ) \
	done
